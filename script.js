"use strict"

const button = document.createElement("button");
button.textContent = "Знайти фізичну адресу по ІP";
document.body.prepend(button);

async function getIpUser() {
    const URL = "https://api.ipify.org/?format=json";
    try {
        const response = await fetch(URL);
        const data = await response.json();
        return data.ip;
    } catch (error) {

        if (error.name !== 'TypeError'){
            throw error
        }
        console.log(error + " Problem with request");

    }
}

async function getAddressInform(ip) {
    const url = `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`;
    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {

        if (error.name !== 'TypeError'){
            throw error
        }
        console.log(error + " Problem with request");
    }
}

async function btnClick() {
    try {
        const ipUser = await getIpUser();
        console.log(ipUser)
        const dataAddress = await getAddressInform(ipUser);

        const div = document.createElement("div");
        button.after(div)

        const ukrainianNames = {
            continent: 'Континент',
            country: 'Країна',
            regionName: 'Регіон',
            city: 'Місто',
            district: 'Район'
        };

        for (const key of Object.keys(dataAddress)) {
            const p = document.createElement("p");
            p.textContent = `${ukrainianNames[key]}: ${dataAddress[key] || 'Не має інформації'}`;
            div.prepend(p);
        }

    } catch (error) {
        console.error("Помилка отримання даних:", error.message);
    }
}

button.addEventListener("click", btnClick)
